################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Flash_LED1.cpp \
../src/Flash_LED2.cpp \
../src/Flash_LED3.cpp \
../src/Flash_LED4.cpp \
../src/Lab1_Functions.cpp \
../src/Lab4Functions.cpp \
../src/Lab4_Refactored_uTTCOSg2016_main.cpp 

ASM_SRCS += \
../src/Lab1LibraryASM.asm 

SRC_OBJS += \
./src/Flash_LED1.doj \
./src/Flash_LED2.doj \
./src/Flash_LED3.doj \
./src/Flash_LED4.doj \
./src/Lab1LibraryASM.doj \
./src/Lab1_Functions.doj \
./src/Lab4Functions.doj \
./src/Lab4_Refactored_uTTCOSg2016_main.doj 

ASM_DEPS += \
./src/Lab1LibraryASM.d 

CPP_DEPS += \
./src/Flash_LED1.d \
./src/Flash_LED2.d \
./src/Flash_LED3.d \
./src/Flash_LED4.d \
./src/Lab1_Functions.d \
./src/Lab4Functions.d \
./src/Lab4_Refactored_uTTCOSg2016_main.d 


# Each subdirectory must supply rules for building sources it contributes
src/Flash_LED1.doj: ../src/Flash_LED1.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Flash_LED1.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Flash_LED2.doj: ../src/Flash_LED2.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Flash_LED2.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Flash_LED3.doj: ../src/Flash_LED3.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Flash_LED3.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Flash_LED4.doj: ../src/Flash_LED4.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Flash_LED4.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1LibraryASM.doj: ../src/Lab1LibraryASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn.exe -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG -i"H:\ENCM511\Lab4_Refactored\system" -gnu-style-dependencies -MM -Mo "src/Lab1LibraryASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1_Functions.doj: ../src/Lab1_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab1_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab4Functions.doj: ../src/Lab4Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab4Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab4_Refactored_uTTCOSg2016_main.doj: ../src/Lab4_Refactored_uTTCOSg2016_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab4_Refactored" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab4_Refactored\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab4_Refactored_uTTCOSg2016_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


