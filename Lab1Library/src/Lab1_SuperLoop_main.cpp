#include <GPIO2016/ADSP_GPIO_Interface.h>     // Check the header file name from Lab. 0
#include "superloop.h"                        // Put prototypes in here
#include <stdio.h>


#define USE_SMITH_GPIO 1    // Those that understand ENUM can use that C++ syntax
#define USE_MY_GPIO 2				// Treat these as "unsigned ints" when prototyping

//#define  WAIT_TIME  0x424242  // Fix this unsigned long int later to cause LED5 to flash
                              // between every 1 / 32 second to 1/ 8 second
#define WAIT_TIME 0x926252

#define SW4 0x08

void main(void) {

	 My_InitLED(USE_SMITH_GPIO);   // Add the prototype to "superloop.h"  void My_InitLED(unsigned int whichGPIO)
     My_WriteLED(USE_SMITH_GPIO, 0);   // // Clear all LEDs
     Reset_Flash_LED5_StateMachine( );
     Reset_Flash_LED6_StateMachine( );  // Similar code as your Lab 0 Reset_Flash_LED5_StateMachine( );

     My_InitSwitches(USE_SMITH_GPIO);

     // Activate TARD_OS
     Wait_Time_USeconds(WAIT_TIME);
     Flash_LED6();
     unsigned char switchBitValue = My_ReadSwitches(USE_SMITH_GPIO);
     switchBitValue = My_ReadSwitchesASM();
     My_WriteLED(USE_SMITH_GPIO, switchBitValue);


     unsigned char My_DriveCommands[40];
     //bool Coast[40];
     unsigned int counter = 0;
     bool SW4_Pressed = 0;

     for (int i = 0; i < 40; i++){
    	 My_DriveCommands[i] = 0;
     	 //Coast[i] = 0;
     }


     int n = 2;

     unsigned long int onesecond = 2;
     unsigned long int twoandhalfsecond = 5;

     unsigned long int current_cycle = 0;
     unsigned long int EXECUTE_LED6 = 2;
     unsigned long int EXECUTE_LED5 = 1;
     unsigned long int EXECUTE_My_Write_DriveCommands = 2;
     unsigned long int EXECUTE_My_Read_DriveCommands = 5;

    while(1){
        // Write drive commands
    	while(1) {
    	    	 for (unsigned long int i = 0; i < 600; i++){
    	    		 current_cycle = i;
    	    		 if(EXECUTE_LED6 < current_cycle){
    	    			 Flash_LED6();
    	    			 EXECUTE_LED6 +=  onesecond;

    	    		 }

    	    		 if(EXECUTE_My_Write_DriveCommands < current_cycle){
    	    			 switchBitValue = My_ReadSwitchesASM();
    	    			 My_WriteLED(USE_SMITH_GPIO, switchBitValue);

    	    			 SW4_Pressed = isSW4Pressed(switchBitValue);

    	    			 if(SW4_Pressed){
    	    				 My_DriveCommands[counter] = switchBitValue;
    	    			 	 counter++;
    	    			 }

    	    		 // Coast
    	    			 if(counter > 3){
    	    				 if(My_DriveCommands[counter-1] == 0x00 &&
    	    						 My_DriveCommands[counter-2] == 0x00 &&
    								 My_DriveCommands[counter-3] == 0x00){
    	    					 	 	 break;
    	    				 }
    	    			 }


    	    			 EXECUTE_My_Write_DriveCommands += onesecond;
    	    		 }

    	    		 Wait_Time_USeconds(WAIT_TIME/n);
    	    	 }
    	    	 break;
    	}

        // Read drive commands
    	unsigned long int max_counter = counter;
    	while(1) {
    	         counter = 0;

    	         current_cycle = 0;
    	         EXECUTE_LED6 = 2;
    	         EXECUTE_LED5 = 1;
    	         EXECUTE_My_Read_DriveCommands = 5;


    	    	 for (unsigned long int i = 0; i < 200; i++){
    	    		 current_cycle = i;

    	    		 if(counter == max_counter)
    	    			 break;

    	    		 if(EXECUTE_LED6 < current_cycle){
    	    			 Flash_LED6();
    	    			 EXECUTE_LED6 +=  onesecond;
    	    		 }

    	    		 if(current_cycle == EXECUTE_My_Read_DriveCommands - 1)
    	    			 Flash_LED5();

    	    		 if(EXECUTE_My_Read_DriveCommands < current_cycle){
    	    			 My_WriteLED(USE_SMITH_GPIO, My_DriveCommands[counter]);
    	    			 counter++;
    	    			 EXECUTE_My_Read_DriveCommands +=  twoandhalfsecond;
    	    			 Flash_LED5();
    	    		 }

    	    		 Wait_Time_USeconds(WAIT_TIME/n);
    	    	 }
    	    	 break;
    	}

    	// Flash LED6 4 times as fast
    	n = 4;
    	bool SW124_Pressed = 0;
    	while(1){
    		switchBitValue = My_ReadSwitchesASM();
    		SW124_Pressed = isSW124Pressed(switchBitValue);
    		if(SW124_Pressed){
    			break;
    		}
    		else
    			Flash_LED6();
    			Wait_Time_USeconds(WAIT_TIME/n);
    	}

        // Repeat commands + Flash LED6 six times as fast
        n = 6;
        current_cycle = 0;
        onesecond = 3;
        twoandhalfsecond = 15;
        EXECUTE_LED6 = 1;
        EXECUTE_LED5 = 3;
        EXECUTE_My_Write_DriveCommands = 6;
        EXECUTE_My_Read_DriveCommands = 15;

    }

}
