################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Lab1LibraryCPP.cpp \
../src/Lab1_Functions.cpp \
../src/Lab1_SuperLoop_main.cpp 

ASM_SRCS += \
../src/Lab1LibraryASM.asm 

SRC_OBJS += \
./src/Lab1LibraryASM.doj \
./src/Lab1LibraryCPP.doj \
./src/Lab1_Functions.doj \
./src/Lab1_SuperLoop_main.doj 

ASM_DEPS += \
./src/Lab1LibraryASM.d 

CPP_DEPS += \
./src/Lab1LibraryCPP.d \
./src/Lab1_Functions.d \
./src/Lab1_SuperLoop_main.d 


# Each subdirectory must supply rules for building sources it contributes
src/Lab1LibraryASM.doj: ../src/Lab1LibraryASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn.exe -file-attr ProjectName="Lab1Library" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG -i"H:\ENCM511\Lab1Library\system" -gnu-style-dependencies -MM -Mo "src/Lab1LibraryASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1LibraryCPP.doj: ../src/Lab1LibraryCPP.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab1Library" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab1Library\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab1LibraryCPP.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1_Functions.doj: ../src/Lab1_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab1Library" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab1Library\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab1_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1_SuperLoop_main.doj: ../src/Lab1_SuperLoop_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn.exe -c -file-attr ProjectName="Lab1Library" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -I"H:\ENCM511\Lab1Library\system" -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab1_SuperLoop_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


