#include <GPIO2016/ADSP_GPIO_Interface.h>     // Check the header file name from Lab. 0
#include "superloop.h"
#include <stdio.h>							//TODO: TAKE OUT AFTER

#define USE_SMITH_GPIO 1    // Those that understand ENUM can use that C++ syntax
#define USE_MY_GPIO 2				// Treat these as "unsigned ints" when prototyping

#define LED6_OFF_MASK	~(0x20)//0xDF	//Bit pattern 1101_1111
#define LED6_ON_MASK	0X20	//Bit pattern 0010_0000

void  My_InitLED(unsigned int whichGPIO) {
     if ( whichGPIO == USE_SMITH_GPIO) {
        Init_LED_GPIOInterface( );
     }
     else {
       printf("My_InitLED with USE_MY_GPIO -- not ready\n");
     }
} 	 // PSP -- REMEMBER CODE REVIEW TIME = 25% OF CODING TIME


void My_InitSwitches(unsigned int whichOS ){
	My_InitSwitchesASM();
}
void Wait_Time_USeconds(unsigned long int waitThisTime) {
//	waitThisTime = 5000;		//adjust this so that wait_Time_USeconds(1 million) is around 1 second

	for (unsigned long int count = 0; count < waitThisTime; count++) {
			//count = count+2;
   }

}

unsigned short int My_ReadSwitches(unsigned int whichGPIO) {
	// Switch register is 16 bits and not 8 bits like LEDs
	     if ( whichGPIO == USE_SMITH_GPIO) {
	    	 //unsigned short int test = Read_Input_GPIOInterface() >> 8;
	    	 //printf("%u",test);
	    	 return Read_Input_GPIOInterface() >> 8;
	     }
	     else {
	            return (My_ReadSwitchesASM( ));

/*
	#if 1
	       printf("MyReadSwitches with USE_MY_GPIO -- not ready\n");
				  unsigned short int garbage = 0xA0A0;  //Return garbage value -- but make it a "known" garbage value
	       return (garbage);
	#else
	       return (My_ReadSwitchesASM( ));
	#endif
*/
	     }
}

void My_WriteLEDs(unsigned int USE_GPIO, unsigned char switchBitValue){
	if (USE_GPIO == USE_SMITH_GPIO)
		Write_Output_GPIOInterface((switchBitValue | Read_LED_GPIOInterface()));
}

enum bitwise_LED6_State {IS_OFF, IS_ON};
static bitwise_LED6_State currentState = IS_OFF;
void Flash_LED6(void) {
	bitwise_LED6_State nextStateToDo = currentState;

	switch (currentState) {
		case IS_OFF:
			Write_LED_GPIOInterface(LED6_ON_MASK | Read_LED_GPIOInterface());
			nextStateToDo = IS_ON;
			break;
		case IS_ON:
			Write_LED_GPIOInterface(LED6_OFF_MASK & Read_LED_GPIOInterface());
			nextStateToDo = IS_OFF;
			break;
	}
	currentState = nextStateToDo;
}

void My_WriteLED(unsigned int whichOS, unsigned char LEDvalue) {
      if (whichOS == USE_SMITH_GPIO) {
           Write_LED_GPIOInterface((Read_LED_GPIOInterface() & 0xF0) | LEDvalue );   // Call my existing code
      }
}

void Reset_Flash_LED6_StateMachine(void) {
	currentState = IS_OFF;
}
