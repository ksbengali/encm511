/*
 * Test_My_ReadSwitchesASM.cpp
 *
 *  Created on: Oct 25, 2016
 *      Author: nkawsar
 */

#define EMBEDDEDUNIT_LITE
#include <EmbeddedUnit2016/EmbeddedUnit2016.h>
//#include "Lab1_Tests_cpp.h"
#include "superloop.h"
#include "stdio.h"

#define USE_SMITH_GPIO 1    // Those that understand ENUM can use that C++ syntax
#define USE_MY_GPIO 2				// Treat these as "unsigned ints" when prototyping

TEST_CONTROL(Testing_MySwitchRoutinesWork_cpp);

TEST(Testing_MySwitchRoutinesWork) {  // Check that the GPIO switch library routines work correctly
	char dummyRead[250];     // Buffer
	My_InitLED(USE_SMITH_GPIO );        // Will NOT CHANGE this during this task
//	My_InitSwitches(USE_SMITH_GPIO );   // Will NOT CHANGE this during this task
	My_InitSwitchesASM();
/*	My_DisableSwitchesCPP();
	//	Display a value to set in switches -- Do a switch read � then do a XF_CHECK to show it fails
	unsigned int switchValue =My_ReadSwitches(USE_MY_GPIO );    // Grab switch value using uTTCOS utility
	CHECK(0 ==  switchValue);

	My_EnableSwitchesCPP ();
	//Display a value to set in switches -- Do a switch read � then do a CHECK to show it passes
	switchValue =My_ReadSwitches(USE_MY_GPIO );    // Grab switch value using uTTCOS utility
	CHECK(0 !=  switchValue);
*/
     unsigned char numbers[ ] = {0,   2,  13,  5,  4};   // Arbitrary numbers � change to your birthday if you want

     #define SWITCHMASK  0xF

     for (int count = 0; count < 3; count++) {
                  printf("Set this value 0x%x as a bit pattern in the 4 switches \n" ,  numbers [count]);
                  printf("Then type �GO� in the pop-up entry window;  �Hey presto works as well\n");
                  printf("and press enter\n  The same bit pattern should appear in LED lights\n");
                  gets(dummyRead);	 // The way to force CCES to get Blackfin to ask for GO command

                                                           // Will change this to USE_MY_GPIO in next test to test your ASM version
                  //My_DisableSwitchesCPP();
                  unsigned int expected = (~numbers [count] & SWITCHMASK);
                  unsigned int switchValue = ((~My_ReadSwitches(USE_MY_GPIO )) & SWITCHMASK);    // Grab switch value using uTTCOS utility
                  My_WriteLED(USE_SMITH_GPIO, switchValue);		          // Echo value to LEDS
                  //CHECK_EQUAL((int) numbers [count], (int) switchValue);
                  CHECK(expected == switchValue);
                  CHECK_EQUAL(expected, switchValue);
                  // From Lab 0 � we know that E-UNIT prints �char� values that are unreadable.
                  	  //CHECK(numbers [count] ==  switchValue);
                  // So use the C++ syntax of �CAST�ing into an integer so that �unsigned char� number values print
                  	  //CHECK_EQUAL((int) numbers [count], (int) switchValue);
     }

     /*for (int count = 0; count < 3; count++) {
                       printf("Set this value 0x%x as a bit pattern in the 4 switches \n" ,  numbers [count]);
                       printf("Then type �GO� in the pop-up entry window;  �Hey presto works as well\n");
                       printf("and press enter\n  The same bit pattern should appear in LED lights\n");
                       gets(dummyRead);	 // The way to force CCES to get Blackfin to ask for GO command

                                                                // Will change this to USE_MY_GPIO in next test to test your ASM version
                       //My_EnableSwitchesCPP ();
                       unsigned int switchValue =My_ReadSwitches(USE_MY_GPIO );    // Grab switch value using uTTCOS utility
                       My_WriteLED(USE_SMITH_GPIO, switchValue);		          // Echo value to LEDS
                       CHECK_EQUAL((int) numbers [count], (int) switchValue);
         // From Lab 0 � we know that E-UNIT prints �char� values that are unreadable.
                       //CHECK(numbers [count] ==  switchValue);
         // So use the C++ syntax of �CAST�ing into an integer so that �unsigned char� number values print
                       //CHECK_EQUAL((int) numbers [count], (int) switchValue);
      }*/

}

