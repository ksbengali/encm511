/*
 * Lab1LibraryCPP.cpp
 *
 *  Created on: Oct 25, 2016
 *      Author: nkawsar
 */

#define EMBEDDEDUNIT_LITE
#include <EmbeddedUnit2016/EmbeddedUnit2016.h>
//#include "Lab1_Tests_cpp.h"
#include "superloop.h"
#include "stdio.h"

#define MASK_PF8TO11_OFF 	0xF0FF
#define MASK_PF8TO11_ON		0x0F00

 void My_DisableSwitchesCPP(void) {
	 *pFIO_INEN &= MASK_PF8TO11_OFF;
	 ssync();
 }

 void My_EnableSwitchesCPP(void) {
	 *pFIO_INEN |= MASK_PF8TO11_ON;
	 ssync( );}

 void My_SetSwitch_MASKA_and_MASKB_InteruptsOffCPP(void) {
	 *pFIO_MASKA_S &= MASK_PF8TO11_OFF;
	 ssync();

	 *pFIO_MASKB_S &= MASK_PF8TO11_OFF;
	 ssync();
 }
