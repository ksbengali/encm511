/*****************************************************************************
 * superloop.h
 *****************************************************************************/

#ifndef __SUPERLOOP_H__
#define __SUPERLOOP_H__

/* Add your custom header content here */

/* C++ function prototypes*/
 void My_InitLED(unsigned int whichOS);
 void My_InitSwitches(unsigned int whichOS );
 void Wait_Time_USeconds(unsigned long int delayMicroSeconds);
 void Flash_LED6(void);
 unsigned short int My_ReadSwitches(unsigned int whichGPIO);
 void My_AdjustSwitchPolarity(void);
 void My_WriteLED(unsigned int whichOS, unsigned char LEDBitPattern);
 unsigned char My_ReadLED(unsigned int whichOS);
 void Reset_Flash_LED6_StateMachine(void);
 void My_WriteLEDs(unsigned int USE_GPIO, unsigned char switchBitValue);
 void My_DisableSwitchesCPP(void);
 extern "C" void My_EnableSwitchesCPP(void);
 extern "C" void My_SetSwitch_MASKA_and_MASKB_InteruptsOffCPP(void);

 /*Assembly function prototypes*/
 extern "C" void My_InitSwitchesASM(void);
 extern "C" int My_ReadSwitchesASM(void);
 extern "C" void My_AdjustSwitchPolarityASM(void);
 extern "C" void My_SetSwitchDirection(void);
 extern "C" void My_SetSwitchSensitivityASM(void);
#endif /* __SUPERLOOP_H__ */
