/*
 * Lab1LibraryASM.asm
 *
 *  Created on: Oct 25, 2016
 *      Author: nkawsar
 */

#include <blackfin.h>

#define SHIFT_MASK 		8
#define FIO_POLAR		0xFFC00734
#define FIO_DIR			0xFFC00730
#define FIO_EDGE		0xFFC00738
#define MASK_PF8TO11 	0xF0FF

	.section program
	.global _My_ReadSwitchesASM
	
_My_ReadSwitchesASM:
	LINK 16;
	.extern _Read_Input_GPIOInterface;
	CALL _Read_Input_GPIOInterface;

	R1 = SHIFT_MASK;
	R0 >>= R1;
_My_ReadSwitchesASM.END:
	UNLINK;
	RTS;
	
	.section program
	.global _My_InitSwitchesASM;
	
_My_InitSwitchesASM:
	LINK 16;
	.extern _Init_Input_GPIOInterface;
	CALL _Init_Input_GPIOInterface;
	
	.extern _My_AdjustSwitchPolarityASM;
	CALL _My_AdjustSwitchPolarityASM;
	
	.extern _My_SetSwitchDirectionASM;
	CALL _My_SetSwitchDirectionASM;
	
	.extern _My_SetSwitch_MASKA_and_MASKB_InteruptsOffCPP;
	CALL _My_SetSwitch_MASKA_and_MASKB_InteruptsOffCPP;
	
	.extern _MySetSwitchSensitivityASM;
	CALL _MySetSwitchSensitivityASM;
	
	.extern _My_EnableSwitchesCPP;
	CALL _My_EnableSwitchesCPP;
_My_InitSwitchesASM.END:
	UNLINK;
	RTS;

	.section program
	.global _My_InitSwitchesASM
	
_My_AdjustSwitchPolarityASM:
	LINK 16;
	P1.L = lo(FIO_POLAR);
	P1.H = hi(FIO_POLAR);
	R1 = MASK_PF8TO11 ( Z );
	R0 = W[P1](Z);
	R0 = R0 & R1;
__My_AdjustSwitchPolarityASM.END:
	UNLINK;
	RTS;
	
	.section program
	.global _My_SetSwitchDirectionASM
	
_My_SetSwitchDirectionASM:
	LINK 16;
	P1.L = lo(FIO_EDGE);
	P1.H = hi(FIO_EDGE);
	R1 = MASK_PF8TO11 ( Z );
	R0 = W[P1](Z);
	R0 = R0 & R1;
_My_SetSwitchDirectionASM.END:
	UNLINK;
	RTS;
	
	.section program
	.global _MySetSwitchSensitivityASM
	
_MySetSwitchSensitivityASM:
	LINK 16;
	P1.L = lo(FIO_DIR);
	P1.H = hi(FIO_DIR);
	R1 = MASK_PF8TO11 ( Z );
	R0 = W[P1](Z);
	R0 = R0 & R1;
_MySetSwitchSensitivityASM.END:
	UNLINK;
	RTS;