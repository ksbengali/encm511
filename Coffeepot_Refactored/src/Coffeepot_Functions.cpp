/*
 * Coffeepot_Functions.cpp
 *
 *  Created on: Nov 21, 2016
 *      Author: Karan
 */

#include "Coffeepot_Functions.h"

#define LED6_ON_MASK	0x20
#define LED6_OFF_MASK	~(0x20)

#define INDIVIDUAL_ASSIGNMENT_MANUAL_UPDATE	1
#define INDIVIDUAL_ASSIGNMENT_TIMER_INTERRUPT_UPDATE	0

// Function to simulate one second passing
void My_SimulateOneSecondPassing_CPP(void){
#if INDIVIDUAL_ASSIGNMENT_MANUAL_UPDATE
	UpdateSimulationDisplay();
#elif INDIVIDUAL_ASSIGNMENT_TIMER_INTERRUPT_UPDATE
	// Do via interrupt control


#else
#error "No assignment type specified"
#endif
}

void My_DemonstrateCoffeePot_ReadyForAction(COFFEEPOT_DEVICE *coffeePot_BaseAddress,char uniqueCoffeePotName[],
		unsigned short int waterLevelRequired,unsigned short int waterTemperatureRequired){

	//Software_Interrupt();

	My_MakeCoffeePot_ReadyForAction(coffeePot_BaseAddress,uniqueCoffeePotName);
	unsigned short int controlRegisterValue = 0;

	// Water and Heater control
	while(1){
		if(CurrentWaterLevel_CPP(coffeePot_BaseAddress) != waterLevelRequired){
			WaterControl_CPP(coffeePot_BaseAddress,waterLevelRequired);
			Write_LED_GPIOInterface(Read_LED_GPIOInterface() | 0x08);
		}
		else{
			Write_LED_GPIOInterface(Read_LED_GPIOInterface() & 0xF7);
		}
		if(CurrentTemperature_CPP(coffeePot_BaseAddress) != waterTemperatureRequired){
			HeaterControl_CPP(coffeePot_BaseAddress,waterTemperatureRequired);
			Write_LED_GPIOInterface(Read_LED_GPIOInterface() | 0x10);
		}
		else{
			Write_LED_GPIOInterface(Read_LED_GPIOInterface() & 0xEF);
		}
		if(CurrentWaterLevel_CPP(coffeePot_BaseAddress) > waterLevelRequired*0.9){
			if(CurrentTemperature_CPP(coffeePot_BaseAddress) > 0.9*waterTemperatureRequired){
				// Turn LED6 ON to display 90% control
				Write_LED_GPIOInterface(Read_LED_GPIOInterface() | 0x20);
			}
			else{
				Write_LED_GPIOInterface(Read_LED_GPIOInterface() & 0xDF);
			}
		}
		//My_SimulateOneSecondPassing_CPP();
	}
}

void My_DemonstrateCoffeePotAction(bool hardwareControl,
		COFFEEPOT_DEVICE *coffeePot1_BaseAddress,char uniqueCoffeePotName1[],
		unsigned short int waterLevelRequired1,unsigned short int waterTemperatureRequired1,
		COFFEEPOT_DEVICE *coffeePot2_BaseAddress,char uniqueCoffeePotName2[],
		unsigned short int waterLevelRequired2,unsigned short int waterTemperatureRequired2){
	// Function Stub
}

void My_MakeCoffeePot_ReadyForAction(COFFEEPOT_DEVICE *coffeePot_BaseAddress,char uniqueCoffeePotName[]){
	// Set Control Register bit INITandSTAYPOWERED ON
	coffeePot_BaseAddress -> controlRegister |= INITandSTAYPOWEREDON_BIT;

	// Wait till Read only Control Register bit DEVICE_READY_RO becomes 1 (after 10 simulated seconds)
	while(coffeePot_BaseAddress -> controlRegister != 0x11){
		//My_SimulateOneSecondPassing_CPP();
	}

	// Enable LED operation -- using BIT-WISE OR
	coffeePot_BaseAddress->controlRegister |=(LED_DISPLAY_ENABLE_BIT | USE_LED1_TO_SHOW_SYSTEM_POWEREDUP);

	// Use LEDs to show system powered up and LEDs enabled
	coffeePot_BaseAddress->controlRegister |=(USE_LED1_TO_SHOW_SYSTEM_POWEREDUP | USE_LED4_TO_SHOW_LED_DISPLAY_ENABLED);

	// Enable water power -- show with LED
	coffeePot_BaseAddress->controlRegister |= WATER_ENABLE_BIT;
	coffeePot_BaseAddress->controlRegister |= (USE_LED3_TO_SHOW_WATER_ENABLED);

	// Enable heater power -- show with LED
	coffeePot_BaseAddress->controlRegister |= HEATER_ENABLE_BIT;
	coffeePot_BaseAddress->controlRegister |= (USE_LED2_TO_SHOW_HEATER_ENABLED);
}

#define SW4_PRESSED 	0x8
bool isSW4Pressed(void){
	unsigned int isSW4_Pressed = 0;
	isSW4_Pressed = (Read_Input_GPIOInterface() >> 8) & 0x8;
	if(isSW4_Pressed == SW4_PRESSED)
		return true;
	else
		return false;
}

#define SW3_PRESSED 	0x4
bool isSW3Pressed(void){
	unsigned int isSW3_Pressed = 0;
	isSW3_Pressed = (Read_Input_GPIOInterface() >> 8) & 0x4;
	if(isSW3_Pressed == SW3_PRESSED)
		return true;
	else
		return false;
}
void InitCoreTimer(void){
	*pTCNTL 	|= 0x00000002;
	*pTCNTL 	|= 0x00000001;
	*pTCNTL 	|= 0x00000004;
	ssync();
}
void Set_IMASK(void){
	*pIMASK |= 0x00000040;
	ssync();
}
void Start_CoreTimer(void){
	*pTSCALE 	= 0x00;
	*pTPERIOD 	= 0x400000;
	*pTCOUNT 	= 0x020000;
	ssync();
}
void Set_EVT_Table(void){
	//*pEVT6 =  UpdateSimulationDisplay;
	*pEVT6 = CoreTimer_ISR;
}
void Software_Interrupt(void){
	InitCoreTimer();
	Set_IMASK();
	Set_EVT_Table();
	Start_CoreTimer();
}
#pragma interrupt
void CoreTimer_ISR(void){
	UpdateSimulationDisplay();
}
