/*
 * HeaterControl_CPP.cpp
 *
 *  Created on: Nov 21, 2016
 *      Author: Karan
 */

#include "Coffeepot_Functions.h"

void HeaterControl_CPP(COFFEEPOT_DEVICE *coffeePot_BaseAddress,unsigned short int waterTemperatureRequired){
	if(isSW3Pressed()){
		if(CurrentTemperature_CPP(coffeePot_BaseAddress) < 79){
			coffeePot_BaseAddress -> heaterBoostRegister = 3;
			coffeePot_BaseAddress -> heaterRegister = 200;
		}
		if(CurrentTemperature_CPP(coffeePot_BaseAddress) > 81){
			//coffeePot_BaseAddress -> waterInFlowRegister = 30;
			coffeePot_BaseAddress -> heaterBoostRegister = 1;
			coffeePot_BaseAddress -> heaterRegister = 200;
		}
	}
	else{
		coffeePot_BaseAddress -> heaterBoostRegister = 0;
		coffeePot_BaseAddress -> heaterRegister = 0;
	}
}
