/*****************************************************************************
 * Coffeepot_Refactored.h
 *****************************************************************************/

#ifndef __COFFEEPOT_REFACTORED_H__
#define __COFFEEPOT_REFACTORED_H__

/* Add your custom header content here */

#include <sys/platform.h>
#include "adi_initialize.h"
#include <stdio.h>

#include <MockDevices2016/CoffeePot_SimulatorFunctions2016.h>
#include <MockDevices2016/CoffeePot_SimulatorStructures.h>

#include <GPIO2016/ADSP_GPIO_Interface.h>
#include "Coffeepot_Functions.h"

#define DO_SECOND_POT				false
#define CONTROL_TWO_POTS_AT_ONCE	false

#endif /* __COFFEEPOT_REFACTORED_H__ */
