/*****************************************************************************
 * Coffeepot_Refactored.cpp
 *****************************************************************************/

#include "CoffeePot_2016main.h"

void NetworkTimingFudge_USE_CCES_GUI_Delay(void){
	// Do nothing
}

void Do_Simulation_CPP(COFFEEPOT_DEVICE * coffeepot_BaseAddress){
	printf("Here in STUB DO_Simulation_CPP\n");
}

int main(void){
	Init_LED_GPIOInterface();
	Init_Input_GPIOInterface();

	Write_LED_GPIOInterface(0);
	Software_Interrupt();

	printf("Blackfin\n\n");
	printf("About to start my coffee pot simulator\n");

	int numCoffeePots = 2;

	WHICHDISPLAY whichdisplay = USE_TEXT_GUI;
//	WHICHDISPLAY whichDisplay = USE_CCES_GUI;
//	WHICHDISPLAY whichDisplay = (WHICHDISPLAY) (USE_TEXT_GUI | USE_CCES_GUI);
//	WHICHDISPLAY whichDisplay = (WHICHDISPLAY) (USE_TEXT_GUI | USE_SPI_GUI);

	Init_CoffeePotSimulation(numCoffeePots, whichdisplay);

	char uniqueCoffeepotName1[] = "coyg";
	char uniqueCoffeepotName2[] = "foys";

	COFFEEPOT_DEVICE *coffeePot1_BaseAddress = (COFFEEPOT_DEVICE *) NULL;
	coffeePot1_BaseAddress = Add_CoffeePotToSystem_PlugAndPlay(COFFEEPOT1,uniqueCoffeepotName1);

	//SimulateOneSecondPassing();

	unsigned short int waterLevelRequired1 = 300;
	unsigned short int waterTemperatureRequired1 = 100;

	My_DemonstrateCoffeePot_ReadyForAction(coffeePot1_BaseAddress,uniqueCoffeepotName1,waterLevelRequired1,waterTemperatureRequired1);

	return 0;
}

