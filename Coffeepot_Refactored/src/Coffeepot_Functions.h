/*
 * Coffeepot_Functions.h
 *
 *  Created on: Nov 21, 2016
 *      Author: Karan
 */

#ifndef COFFEEPOT_FUNCTIONS_H_
#define COFFEEPOT_FUNCTIONS_H_

#include "CoffeePot_2016main.h"
#include "stdio.h"
#include <GPIO2016/ADSP_GPIO_Interface.h>

#define RESET			0x0000001F;

void SimulateOneSecondPassing(void);

void My_DemonstrateCoffeePot_ReadyForAction(COFFEEPOT_DEVICE *coffeePot_BaseAddress,char uniqueCoffeePotName[],
		unsigned short int waterLevelRequired,unsigned short int waterTemperatureRequired);

void My_DemonstrateCoffeePotAction(bool hardwareControl,
		COFFEEPOT_DEVICE *coffeePot1_BaseAddress,char uniqueCoffeePotName1[],
		unsigned short int waterLevelRequired1,unsigned short int waterTemperatureRequired1,
		COFFEEPOT_DEVICE *coffeePot2_BaseAddress,char uniqueCoffeePotName2[],
		unsigned short int waterLevelRequired2,unsigned short int waterTemperatureRequired2);

void My_MakeCoffeePot_ReadyForAction(COFFEEPOT_DEVICE *coffeePot_BaseAddress,char uniqueCoffeePotName[]);

void WaterControl_CPP(COFFEEPOT_DEVICE *coffeePot_BaseAddress,unsigned short int waterLevelRequired);

void WaterControl_ASM(COFFEEPOT_DEVICE *coffeePot_BaseAddress,unsigned short int waterLevelRequired);

void HeaterControl_CPP(COFFEEPOT_DEVICE *coffeePot_BaseAddress,unsigned short int waterTemperatureRequired);

void HeaterControl_ASM(COFFEEPOT_DEVICE *coffeePot_BaseAddress,unsigned short int waterTemperatureRequired);

void My_SimulateOneSecondPassing_CPP(void);

bool isSW4Pressed(void);

bool isSW3Pressed(void);

void Set_IMASK(void);
void Start_CoreTimer(void);
void Set_EVT_Table(void);
void Clear_Interrupt(void);
void Software_Interrupt(void);

extern "C" void CoreTimer_ISR_ASM(void);

#pragma interrupt
extern "C" void CoreTimer_ISR(void);


#endif /* COFFEEPOT_FUNCTIONS_H_ */
