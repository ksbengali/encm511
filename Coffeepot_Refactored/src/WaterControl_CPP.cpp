/*
 * WaterControl_CPP.cpp
 *
 *  Created on: Nov 21, 2016
 *      Author: Karan
 */
#include "Coffeepot_Functions.h"

void WaterControl_CPP(COFFEEPOT_DEVICE *coffeePot_BaseAddress,unsigned short int waterLevelRequired){
	 if(isSW4Pressed()){
		// Turn LED4 ON to display water in flow
		if(CurrentTemperature_CPP(coffeePot_BaseAddress) < 79)
			coffeePot_BaseAddress->waterInFlowRegister = 30;
		else
			coffeePot_BaseAddress->waterInFlowRegister = 20;
	}
	 else{
		 coffeePot_BaseAddress->waterInFlowRegister = 0;
	 }
}
