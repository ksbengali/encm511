/*****************************************************************************
 * Assignment2_ISR.cpp
 *****************************************************************************/
#include "Assignment2_ISR.h"

volatile int timerSignal  = 0;
volatile int SWIsignal = 0;

void main(void){
	My_InitSwitches();
	InitLED();
	Write_LED_GPIOInterface(0);

	Software_Interrupt();

	while(1){
		Waste1Sec();
		if(isSW4ON()){
			Flash_LED1();
		}
		if(isSW3ON()){
			Flash_LED5();
		}
	}
	return;
}
