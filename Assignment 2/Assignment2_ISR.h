/*****************************************************************************
 * Assignment2_ISR.h
 *****************************************************************************/

#ifndef __ASSIGNMENT2_ISR_H__
#define __ASSIGNMENT2_ISR_H__

/* Add your custom header content here */

#include <GPIO2016/ADSP_GPIO_Interface.h>
#include "superloop.h"
#include <blackfin.h>
#include <stdio.h>
#include <sys/platform.h>
#include "adi_initialize.h"

extern volatile int timerSignal;
extern volatile int SWIsignal;

void Flash_LED6(void);
void Flash_LED3(void);
void Flash_LED1(void);
void Waste1Sec(void);
void My_InitSwitches(void);
void InitLED();

bool isSW4ON(void);
bool isSW3ON(void);

void InitCoreTimer(void);
void Set_IMASK(void);
void Start_CoreTimer(void);
void Set_EVT_Table(void);
void Clear_Interrupt(void);
void Software_Interrupt(void);

#pragma interrupt
void CoreTimer_ISR(void);

#endif /* __ASSIGNMENT2_ISR_H__ */
