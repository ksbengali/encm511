/*
 * Assign2_Functions.cpp
 *
 *  Created on: Nov 22, 2016
 *      Author: ksbengal
 */
#include "Assignment2_ISR.h"


#define LED1_ON_MASK	0x01
#define LED1_OFF_MASK	~(0x01)

#define LED3_ON_MASK	0x04
#define LED3_OFF_MASK	~(0x04)

#define LED6_ON_MASK	0x20
#define LED6_OFF_MASK	~(0x20)

#define SW4_Pressed 	0x08
#define SW3_Pressed 	0x04

#define WAIT_TIME 		0x926252

#define IVTMR			0x00000040
#define RESET			0x0000001F;

enum bitwise_LED_State {IS_OFF, IS_ON};
static bitwise_LED_State currentState1 = IS_OFF;
static bitwise_LED_State currentState3 = IS_OFF;
static bitwise_LED_State currentState6 = IS_OFF;

void Flash_LED6(void){
	bitwise_LED_State nextStateToDo6 = currentState6;
	switch (currentState6) {
	case IS_OFF:
		Write_LED_GPIOInterface(LED6_ON_MASK | Read_LED_GPIOInterface());// << (3 - 1));  // Was off turn it on
		nextStateToDo6 = IS_ON;
		break;
	case IS_ON:
		Write_LED_GPIOInterface(LED6_OFF_MASK & Read_LED_GPIOInterface());// << (3 - 1)); // Turn off LED 3
		nextStateToDo6 = IS_OFF;
	break;
	}
	currentState6 = nextStateToDo6;
}
void Flash_LED3(void){
	bitwise_LED_State nextStateToDo3 = currentState3;
	switch (currentState3) {
	case IS_OFF:
		Write_LED_GPIOInterface(LED3_ON_MASK | Read_LED_GPIOInterface());// << (3 - 1));  // Was off turn it on
		nextStateToDo3 = IS_ON;
		break;
	case IS_ON:
		Write_LED_GPIOInterface(LED3_OFF_MASK & Read_LED_GPIOInterface());// << (3 - 1)); // Turn off LED 3
		nextStateToDo3 = IS_OFF;
	break;
	}
	currentState3 = nextStateToDo3;
}
void Flash_LED1(void){
	bitwise_LED_State nextStateToDo1 = currentState1;
	switch (currentState1) {
	case IS_OFF:
		Write_LED_GPIOInterface(LED1_ON_MASK | Read_LED_GPIOInterface());// << (3 - 1));  // Was off turn it on
		nextStateToDo1 = IS_ON;
		break;
	case IS_ON:
		Write_LED_GPIOInterface(LED1_OFF_MASK & Read_LED_GPIOInterface());// << (3 - 1)); // Turn off LED 3
		nextStateToDo1 = IS_OFF;
	break;
	}
	currentState1 = nextStateToDo1;
}
void Waste1Sec(void){
	for (unsigned long int count = 0; count < WAIT_TIME; count++);
}
void My_InitSwitches(){
	My_InitSwitchesASM();
}
void InitLED(){
	Init_LED_GPIOInterface();
}
bool isSW4ON(void){
	if((My_ReadSwitchesASM() & SW4_Pressed) == SW4_Pressed)
		return true;
	else
		return false;
}
bool isSW3ON(void){
	if((My_ReadSwitchesASM() & SW3_Pressed) == SW3_Pressed)
		return true;
	else
		return false;
}
void InitCoreTimer(void){
	*pTCNTL 	|= 0x00000002;
	*pTCNTL 	|= 0x00000001;
	*pTCNTL 	|= 0x00000004;
	ssync();
}
void Set_IMASK(void){
	*pIMASK |= 0x00000040;
	ssync();
}
void Start_CoreTimer(void){
	*pTSCALE 	= 0x00;
	*pTPERIOD 	= 0x2000000;
	*pTCOUNT 	= 0x040000;
	ssync();
}
void Set_EVT_Table(void){
	*pEVT6 =  CoreTimer_ISR;
}
void Clear_Interrupt(void){
	*pIMASK = *pIMASK & (~0x00000040);
	ssync();
}
void Software_Interrupt(void){
	InitCoreTimer();
	Set_IMASK();
	Set_EVT_Table();
	Start_CoreTimer();
}
#pragma interrupt
void CoreTimer_ISR(void){
	Flash_LED3();
	//printf("Interrupt Task \n");
}
